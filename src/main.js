import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import firebase from "firebase";
//Globall
import "./assets/Style/style.scss";


const firebaseConfig = {
  apiKey: "AIzaSyA_xXTvutoMrFAJFdHa78FVYjYi_waTmJU",
  authDomain: "vue-auth-6672d.firebaseapp.com",
  projectId: "vue-auth-6672d",
  storageBucket: "vue-auth-6672d.appspot.com",
  messagingSenderId: "597421196102",
  appId: "1:597421196102:web:96af31581d4d30b30a5b47"
};


firebase.initializeApp(firebaseConfig);

createApp(App).use(router).mount("#app");
